import { useParams } from 'react-router-dom';

function Test() {
	const { id, name } = useParams();

	return (
		<div>Test Page {id} {name}</div>
	);
}

export default Test;