import { Form } from 'react-bootstrap';

function Search({ search }) {

	return (
		<Form.Control 
			placeholder="Search task" 
			onChange={(event) => search(event.target.value)}
		/>
	);
}

export default Search;