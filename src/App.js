import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddTask from './AddTask';
import TaskList from './TaskList';
import Search from './Search';
import { useState } from 'react';

import { Container } from 'react-bootstrap';

function App() {
  const [{ todo, done }, setList] = useState({ todo: [], done: [] });
  const [searchList, setSearchList] = useState({ todo, done });
  const [isSearching, setIsSearching] = useState(false);

  console.log('list', todo, done);

  const onSubmit = (value) => {
    setList({ todo: [...todo, value], done });
  };

  const onCheck = (checked, index) => {
    if (checked) { //true/checked
      const todoValue = todo[index]; // value from todo
      todo.splice(index, 1); // remove item in array using index
      setList({ 
        done: [...done, todoValue],
        todo,
      });
    } else {
      const doneValue = done[index];
      done.splice(index, 1); 
      setList({
        done,
        todo: [...todo, doneValue],
      });
    }
  };

  const onRemove = (index, type) => {
    if (type === 'ToDo') {
      todo.splice(index, 1);
    } else {
      done.splice(index, 1);
    }
    setList({ todo, done });
  };

  const onSearch = (searchValue) => {
    console.log('search', searchValue);

    if (searchValue) {
      setIsSearching(true);
      // setSearchList({
      //   todo: todo.filter((item) => item.startsWith(searchValue)),
      //   done: done.filter((item) => item.startsWith(searchValue)),
      // });
      setSearchList({
        todo: todo.filter((item) => item.includes(searchValue)),
        done: done.filter((item) => item.includes(searchValue)),
      });
    } else {
      setIsSearching(false);
    }
  };

  const testId = 3;
  const name = 'ryd';

  return (
    <Container fluid>
      <AddTask submit={onSubmit} />
      <Search search={onSearch} />
      <TaskList
        items={isSearching ? searchList.todo : todo}
        type="ToDo"
        check={onCheck}
        remove={onRemove}
      />
      <TaskList
        items={isSearching ? searchList.done : done}
        type="Done"
        check={onCheck}
        remove={onRemove}
      />
      <a href={`/test/${testId}/${name}`}>Open test 3</a>
    </Container>
  );
}

export default App;
