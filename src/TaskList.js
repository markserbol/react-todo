import { ListGroup, Form, Button, Row, Col } from 'react-bootstrap';

function TaskList({ items, type, check, remove }) {

	const itemList = items.map((item, index) => {
		return (
			<ListGroup.Item key={index}>
				<Row>
					<Col xs="8">
						<Form.Group controlId="checkbox">
							<Form.Check
								onChange={(event) => check(event.target.checked, index)}
								checked={type === 'ToDo' ? false : true }
								label={item}
							/>
						</Form.Group>
					</Col>
					<Col xs="4">
						<Button
							variant="danger"
							size="sm"
							onClick={() => remove(index, type)}
							style={{ float: 'right' }}
						>
							Remove
						</Button>
					</Col>
				</Row>
			</ListGroup.Item>
		);
	});

	return (
		<div style={{ paddingTop: 20 }}>
			<h4>{type}</h4>
			<ListGroup>{itemList}</ListGroup>
		</div>
	);
};

export default TaskList;