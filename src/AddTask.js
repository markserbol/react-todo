import { useState } from 'react';
import { Form, Row, Col, Button } from 'react-bootstrap';

function AddTask({ submit }) {
	const [value, setValue] = useState('');
	const onSubmit = (event) => {
		event.preventDefault();
		submit(value);
		setValue(''); // clear value
	};

	return (
		<form onSubmit={onSubmit} style={{paddingBottom: 20, paddingTop: 20 }}>
			<Row>
				<Col xs="9">
					<Form.Control
						placeholder="Enter task"
						onChange={(event) => setValue(event.target.value)}
						value={value}
					/>
				</Col>
				<Col xs="3">
					<Button variant="outline-primary">Add</Button>
				</Col>
			</Row>
		</form>
	);
}

export default AddTask;
